<?php
/**
 * FAE 
 */
namespace FAE\fields;

use FAE\schema\model\model;
use FAE\schema\model\schema;

use FAE\rest\rest;

use Doctrine\DBAL\Query\QueryBuilder;

class data extends model {
  
  var $_variables;
  var $_searchCols = ['data' => 1000];
  
  function __construct( string $datasource = 'default', array $variables = [] )
  {
    $this->_variables = $variables;
    $this->_model     = ( $this->_variables['_model'] ? $this->_variables['_model'] : str_replace( '-', '_', $this->_variables['_method'] ) );
    $this->_table     = $this->_model;
    
    if(!$this->_model){
      throw new \Exception('Could not load fields\data class due to lack of model definition in variables');
    }
    
    parent::__construct( $datasource, $variables );
  }
  
  function getQueryHook( QueryBuilder $qb, array $filter )
  {
    $qb->leftJoin( 't1', preg_replace( '/(_data)?(_revision)?/', '', $this->_table ), 't2', 't1.`field_id` = t2.`id`' );
    if($filter['ref']){
      $qb->andWhere('t2.`ref` = '.$qb->createNamedParameter($filter['ref']));
    }
    $qb->addOrderBy('t2.`weight`', 'ASC');
    return $qb;
  }
  
  static function loadDataQueries( array $filter, array $columns = [], $instance = null)
  {
    if(is_string($instance)){
      $table = $instance;
    } else {
      $table = $instance->_table;
    }
    $queries = [];
    
    // Test whether data is actually available to be pulled
    $schema = new schema();
    $schema->loadSchema();
    
    if( !array_key_exists( $table.'_field_data', $schema->_models ) ){
      return $queries;
    }
    
    $dataInstance = new self( 'default', [ '_model' => $table.'_field_data' ] );
    $queries[$table.'_field_data'] = $dataInstance->get( $filter, array_merge($dataInstance->_modelData->columnList, ['t2.`ref`', 't2.`name`']) );
    return $queries;
  }
  
  static function loadData( array $variables )
  {
    if( !array_key_exists( '_method', $variables ) ){
      return $variables;
    }
    if( !is_array($variables['data']) ){
      return $variables;
    }
    // Test whether data is actually available to be pulled
    $schema = new schema();
    $schema->loadSchema();
    if( !array_key_exists( $variables['_method'].'_field_data', $schema->_models ) ){
      return $variables;
    }
    
    $instance = new self( 'default', [ '_model' => $variables['_method'].'_field_data' ] );
    
    foreach( $variables['data'] as $k => $row ){
      $data = $instance->get( [ $variables['_method'].'_id' => $row['id'] ], array_merge($instance->_modelData->columnList, ['t2.`ref`', 't2.`name`']) );
      while( $item = $data->fetch() ){
        $variables['data'][$k][$variables['_method'].'_field_data'][$item['field_id']] = $item;
        if(strlen($item['ref'])){
          $variables['data'][$k][$variables['_method'].'_field_data'][$item['ref']] = $item;
        }
      }
    }
    
    return $variables;
  }
  
  static function storeData( $instance, array $data, $parentId, \stdClass $parentModel, array $tableData )
  {
    $table = $tableData['table'];
    $instance = new self( 'default', ['_model' => $table] );
    
    if( !array_key_exists( $table, $data ) ){
      return false;
    }
    
    $parentTable  = preg_replace('/_revision$/', '', $parentModel->table);
    $fieldInstance = new fieldInstance( 'default', ['_model' => $parentTable.'_field'] );
    
    $fields = $fieldInstance->get();
    
    // Setup default values
    while($field = $fields->fetch()){
      $options = json_decode($field['options']);
      // Ignore those without a default value
      if(!$options->defaultValue){
        continue;
      }
      // If we're in array formatted data mode
      if(is_array(current($data[$table])) && array_key_exists('data', current($data[$table]))){
        // Skip pre-formatted array structure
        foreach($data[$table] as $row){
          if(is_array($row) && array_key_exists('field_id', $row) && $row['field_id'] == $field['id']){
            continue 2;
          }
        }
        $data[$table][$field['id']] = [
          $parentTable.'_id'  => $parentId,
          'field_id'          => $field['id'],
          'data'              => $options->defaultValue,
        ];
      // If we're in flat data formatted mode
      } else {
        // Skip those with key matches
        if(array_key_exists($field['id'], $data[$table]) || array_key_exists($field['ref'], $data[$table])){
          continue;
        }
        $data[$table][$field['id']] = $options->defaultValue;
      }
    }
    
    $mergeData = $data[$table];
    foreach($mergeData as $key => $v){
      if(is_int($key) || $key == 'id'){
        unset($mergeData[$key]);
      }
    }
    
    foreach( $data[$table] as $fieldId => $value ){
      if(!is_numeric($fieldId)){
        // test if submitting data by reference
        $field = $fieldInstance->get( ['ref' => $fieldId] );
        if($field->rowCount()){
          $fieldId = $field->fetch()['id'];
        } else {
          continue;
        }
      }
      if(is_string($value) || is_numeric($value) || (is_array($value) && !array_key_exists('field_id', $value))){
        $formatted = array_merge([
          $parentTable.'_id'  => $parentId,
          'field_id'          => $fieldId,
          'data'              => $value,
        ], $mergeData);
      } elseif(is_array($value)){
        if( array_key_exists('field_id', $value) && is_numeric($value['field_id']) ){
          $formatted = $value;
        }
      }
      if(is_array($formatted['data'])){
        $formatted['data'] = json_encode($formatted['data']);
      }
      $checkData = $formatted;
      unset($checkData['data']);
      $check = $instance->get($checkData);
      if( $check->rowCount() > 0 ){
        $instance->update( $formatted, $checkData );
      } else {
        $instance->insert($formatted);
      }
    }
  }
  
  static function fieldOptions( $id, string $target, array $filter = [] )
  {
    if(!isset($id)){
      throw new \Exception('Field ID not passed');
    }
    if(!is_string($target)){
      throw new \Exception('_target table not defined');
    }
    
    $filterID = is_numeric($id) ? ['field_id' => $id] : ['ref' => $id];
    $queries = self::loadDataQueries( array_merge($filter, $filterID), [], $target );
    
    $options = [];
    
    foreach($queries as $query){
      while($row = $query->fetch()){
        $options = array_unique(array_merge($options, explode(',', $row['data'])));
      }
    }
    
    $options = array_filter(array_map('trim', $options));
    
    return $options;
  }
  
  static function fieldOptionsRest(array $variables){
    try {
      $filter = (array) $_GET['filter'];
      $options = self::fieldOptions($variables['id'], $variables['_target'], $filter);
      if(array_key_exists('search', $filter)){
        $options = array_filter($options, function($item){
          return preg_match('/'.preg_quote($_GET['filter']['search']).'/i', $item) === 1 ? true : false;
        });
      }
      $data = rest::formatData( $options, count($options), 0, 0);
      rest::output($data);
    } catch (\Exception $e){
      rest::errorDisplay($e);
    }
  }
  
}