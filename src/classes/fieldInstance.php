<?php
/**
 * FAE 
 */
namespace FAE\fields;

use FAE\schema\model\model;

class fieldInstance extends model {
  
  var $_variables;
  var $_defaultSort = ['weight' => 'ASC'];
  
  function __construct( string $datasource = 'default', array $variables )
  {
    $this->_variables = $variables;
    $this->_model     = $this->_variables['_model'];
    
    parent::__construct( $datasource, $variables );
  }
  
  function update( array $data = array(), array $filter = array(), $escapeVars = true )
  {
    if( array_key_exists( 'create', $data ) ){
      foreach($data['create'] as $field){
        if( ( array_key_exists( 'name', $field ) && strlen($field['name']) ) || ( array_key_exists( 'parent_id', $field ) && $field['parent_id'] > 0 )  ){
          if($field['parent_id'] == 0){
            $field['parent_id'] = null;
          }
          if(is_array($field['options'])){
            $field['options'] = json_encode($field['options']);
          }
          $this->insert( $field, $filter );
        }
      }
    }
    if( array_key_exists( 'update', $data ) ){
      foreach($data['update'] as $field){
        if( is_array($field) && array_key_exists( 'id', $field ) && $this->count( [ "id" => $field['id'] ] ) ){
          if(is_array($field['options'])){
            $field['options'] = json_encode($field['options']);
          }
          $this->set( $field, [ "id" => $field['id'] ], 'update' );
        }
      }
    }
  }
  
}