<?php
/**
 * FAE 
 */
namespace FAE\fields;

use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

use FAE\fae\fae;
use FAE\fae\frames;

class field {
  
  static $_frames;
  static $_models;
  static $_routes;
  static $_targets = [];
  static $fieldModelRaw;
  static $dataModelRaw;
  static $assocModelRaw;
  
  var $error;
  
  const CUSTOM        = 0;
  
  const HIDDEN        = 20;
  
  const TEXT_SHORT    = 1;
  const TEXT_LONG     = 2;
  const TEXT_NUMERIC  = 3;
  const TEXT_URI      = 15;
  const TEXT_DATETIME = 16;
  const TEXT_TAGS     = 17;
  const TEXT_EMAIL    = 18;
  const WYSIWYG       = 11;
  
  const BOOL_SIMPLE   = 4;
  const BOOL_EXT      = 5;
  
  const SELECT_SIMPLE = 6;
  const SELECT_SCORE  = 9;
  
  const COLOR         = 19;
  const PUBLICATIONS  = 7;
  const FILE          = 8;
  const RECAPTCHA     = 21;
  
  const TYPES = array(
    self::CUSTOM        => "Custom",
    
    self::TEXT_SHORT    => "Short Text",
    self::TEXT_LONG     => "Long Text",
    self::TEXT_NUMERIC  => "Number",
    self::TEXT_URI      => "URI",
    self::TEXT_DATETIME => "Date/Time",
    self::TEXT_TAGS     => "Taggable Text",
    self::WYSIWYG       => "HTML Editor",
    self::TEXT_EMAIL    => "Email Address",
    
    self::BOOL_SIMPLE   => "Yes/No",
    self::BOOL_EXT      => "Yes (Longtext)/No (Null)",
    
    self::SELECT_SIMPLE => "Drop-Down List",
    self::SELECT_SCORE  => "Score",
    
    self::COLOR         => "Colour Wheel",
    self::PUBLICATIONS  => "Publication List (PubMed)",
    self::FILE          => "Files",
    self::RECAPTCHA     => "ReCaptcha",
  );
  
  static function routes()
  {
    global $config;

    if(empty(self::$_models)){
      self::loadSchema();
    }
    
    if(!self::$_routes){
      self::$_routes = new RouteCollection();
    }
    
    foreach( self::$_targets as $model ){
      $routable = str_replace( ['_'], '-', $model->table );
      $route = new Route(
        '/adm/field/'.$routable,
        [
          '_dataController' => __NAMESPACE__.'\\fieldInstance',
          '_target' => $model->table,
          '_model' => $model->table.'_field',
          '_page' => 'field.admin.update.html.twig',
          '_layoutPath'   => fae::_path(APPROOT.'/frames/ui_admin/src/views/'),
          '_loadData' => true,
          '_loadChildren' => false,
          '_controller' => '\\FAE\\ui_admin\\admin::pageLoader',
        ],
        [],
        [],
        '',
        [],
        ['GET']
      );
      self::$_routes->add( 'fields-'.$routable.'-view', $route );
      $route = new Route(
        "/api/{$config->apiVersion}/field/{$routable}",
        [
          '_dataController' => __NAMESPACE__.'\\fieldInstance',
          '_target' => $model->table,
          '_model' => $model->table.'_field',
          '_method' => $routable,
          '_action' => 'read',
          '_controller' => '\\FAE\\rest\\rest::routeLoader',
        ],
        [],
        [],
        '',
        [],
        ['GET']
      );
      self::$_routes->add( 'fields-'.$routable.'-rest-read', $route );
      $route = new Route(
        "/api/{$config->apiVersion}/{$routable}-field",
        [
          '_dataController' => __NAMESPACE__.'\\fieldInstance',
          '_target' => $model->table,
          '_model' => $model->table.'_field',
          '_method' => $routable,
          '_action' => 'read',
          '_controller' => '\\FAE\\rest\\rest::routeLoader',
        ],
        [],
        [],
        '',
        [],
        ['GET']
      );
      self::$_routes->add( 'fields-'.$routable.'-rest-read-direct', $route );
      $route = new Route(
        "/api/{$config->apiVersion}/field/{$routable}/data",
        [
          '_dataController' => __NAMESPACE__.'\\data',
          '_target' => $model->table,
          '_model' => $model->table.'_field_data',
          '_method' => $routable,
          '_action' => 'read',
          '_controller' => '\\FAE\\rest\\rest::routeLoader',
        ],
        [],
        [],
        '',
        [],
        ['GET']
      );
      self::$_routes->add( 'fields-data-'.$routable.'-rest-read', $route );
      $route = new Route(
        "/api/{$config->apiVersion}/field/{$routable}",
        [
          '_dataController' => __NAMESPACE__.'\\fieldInstance',
          '_target' => $routable,
          '_model' => $model->table.'_field',
          '_action' => 'update',
          '_controller' => '\\FAE\\rest\\rest::routeLoader',
        ],
        [],
        [],
        '',
        [],
        ['POST','PUT','PATCH']
      );
      self::$_routes->add( 'fields-'.$routable.'-rest-update', $route );
      $route = new Route(
        "/api/{$config->apiVersion}/field/{$routable}/{id}/options",
        [
          '_target' => $routable,
          '_model' => $model->table.'_field',
          '_controller' => '\\FAE\\fields\\data::fieldOptionsRest',
        ],
        ['id' => '[^\/]+'],
        [],
        '',
        [],
        ['GET']
      );
      self::$_routes->add( 'fields-'.$routable.'-rest-read-options', $route );
    }
    
    self::$_routes->setSchemes(['https','http']);
    
    return self::$_routes;
  }
  
  static function loadFields( array $variables ){
    if (!empty($variables['_method'])) {
      $table = str_replace('-','_', $variables['_method']);
    }
    if( !empty($table) && array_key_exists( $table, self::$_targets ) ){
      $variables['_model'] = $table.'_field';
      $fieldInstance = new fieldInstance( 'default', $variables );
      // filter on assoc needed
      $fields = $fieldInstance->get();
      while($row = $fields->fetch()){
        if(is_string($row['options'])){
          $row['options'] = json_decode($row['options'], true);
        }
        $variables['fields'][] = $row;
      }
    }
    return $variables;
  }
  
  static function loadSchema( array $models = [] )
  {
    $framesRef = new frames();
    self::$_frames = $framesRef->loadFrames();
    self::$_models = $models;
    
    foreach( self::$_frames as $frame => $options ){
      if( is_object($options) && property_exists($options, 'fae') && property_exists($options->fae, 'fields') && property_exists($options->fae->fields, 'models')){
        foreach( $options->fae->fields->models as $model ){
          if( !array_key_exists( $model->table, self::$_targets ) ){
            self::$_targets[$model->table] = $model;
            if( $return = self::loadModels( $model->table, $frame, $model ) ){
              self::$_models = array_merge( self::$_models, $return );
            }
          }
        }
      }
    }
    
    return self::$_models;
  }
  
  static function loadModels( string $table, string $frame = null, $options = null )
  {
    $models = [];
    $models = array_merge( $models, self::loadModel($table.'_field') );
    $models = array_merge( $models, self::loadModel($table.'_data') );
    
    if( property_exists( $options, 'assoc' ) ){
      $models = array_merge( $models, self::loadModel( $table.'_assoc', $options->assoc ) );
    }
    
    return $models;
  }
  
  static function loadModel( string $model, string $assocTable = '' )
  {
    $modelPath = explode( '_', $model );
    $type = array_pop($modelPath);
    $table = implode( '_', $modelPath );
    
    if( $type == 'field' ){
      if( !self::$fieldModelRaw ){
        try {
          self::$fieldModelRaw = file_get_contents( fae::_path(__DIR__.'/../data/field.model.json' ) );
        } catch (\Exception $e) {
          die('could not load field model structure');
          return false;
        }
      }
      $modelRaw = self::$fieldModelRaw;
    }
    if( $type == 'data' ){
      if( !self::$dataModelRaw ){
        try {
          self::$dataModelRaw = file_get_contents( fae::_path(__DIR__.'/../data/data.model.json' ) );
        } catch (\Exception $e) {
          die('could not load data model structure');
          return false;
        }
      }
      $modelRaw = self::$dataModelRaw;
    }
    if( $type == 'assoc' ){
      if( !self::$assocModelRaw ){
        try {
          self::$assocModelRaw = file_get_contents( fae::_path(__DIR__.'/../data/assoc.model.json' ) );
        } catch (\Exception $e) {
          die('could not load data model structure');
          return false;
        }
      }
      $modelRaw = self::$assocModelRaw;
    }
    
    $variables = [
      '{{parent_table}}'        => $table,
      '{{field_table}}'         => $table.'_field',
      '{{data_table}}'          => $table.'_field_data',
      '{{data_rel_col}}'        => $table.'_id',
      '{{assoc_table}}'         => $table.'_field_assoc',
      '{{assoc_id}}'            => $table.'_id',
      '{{assoc_relate_table}}'  => $assocTable,
    ];
    
    $result = json_decode( strtr( $modelRaw, $variables ) );
    
    return [ $result->table => $result ];
  }
  
}